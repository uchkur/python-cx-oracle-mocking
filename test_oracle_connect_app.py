from unittest.mock import  patch, MagicMock
from unittest import TestCase, mock
import oracle_connect_app as app

BLOB = """HEADER1 HEADER2 HEADER3 HEADER4
LINE1 LINE1 LINE1 LINE1
LINE2 LINE1 LINE1 LINE1
LINE3 LINE1 LINE1 LINE1
LINE4 LINE1 LINE1 LINE1"""

class TestOracleConnect(TestCase):

    @mock.patch('oracle_connect_app.cx_Oracle', autospec=False) 
    def test_insert(self, mock_ora):
        mock_cursor = mock.MagicMock()
        mock_result = mock.MagicMock()
        mock_cursor.__enter__.return_value = mock_result
        mock_cursor.__exit__ = mock.MagicMock()
        mock_ora.connect.return_value.cursor.return_value.__enter__.return_value = mock_cursor
        app.insert(1, BLOB)
        mock_ora.connect.assert_called_with('appdb', 'myawesomepassword', 'localhost/orclpdb1', encoding='UTF-8')
        mock_cursor.execute.assert_called_with("insert into ldr_body values (:id, :body)", [1, BLOB] )


    @mock.patch('oracle_connect_app.cx_Oracle', autospec=True) 
    def test_get_blob_data(self, mock_ora):
        mock_cursor = mock.MagicMock()
        test_data = [{'body': BLOB, 'id': 1}]
        test_data1 = [{'body': BLOB, 'id': 1}]
        mock_cursor.fetchone.return_value = test_data1
        mock_ora.connect.return_value.cursor.return_value.__enter__.return_value = mock_cursor
        self.assertEqual(test_data, app.get_blob_data(1))

    @mock.patch('oracle_connect_app.cx_Oracle', autospec=True) 
    def test_get_data(self, mock_ora):
        mock_cursor = mock.MagicMock()
        test_data = [{'b': BLOB, 'id': 1}]
        test_data1 = [{'b': BLOB, 'id': 1}]
        mock_cursor.fetchall.return_value = test_data1
#        mock_ora.connect.return_value.__enter__.return_value = mock_cursor
        mock_ora.connect.return_value.cursor.return_value.__enter__.return_value = mock_cursor
        self.assertEqual(test_data, app.get_data())
